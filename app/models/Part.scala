package models

/**
 * Created by mlewin on 8/4/14.
 */
case class Part (id: Long, title: String, url: String)

object Part {

  var demoParts = Set (
    Part(1, "David Gonzalez: Possessed To Skate", "https://www.youtube.com/watch?v=8MqsMop9Q_A"),
    Part(2, "John Motta: A Happy Medium", "https://www.youtube.com/watch?v=Jt5LU7wtFHQ")
    )

  def findAll = demoParts.toList.sortBy(_.id)

  def findById(id: Long) = demoParts.find(_.id == id)

  def add(part: Part){
    demoParts = demoParts + part
  }

}
