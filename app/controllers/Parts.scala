package controllers

/**
 * Created by mlewin on 8/5/14.
 */

import play.api.mvc.{Action, Controller}
import play.api.data.Form
import play.api.data.Forms.{mapping, longNumber, nonEmptyText}
import play.api.i18n.Messages
import play.api.mvc.Flash

import models.Part



object Parts extends Controller {
  def list = Action { implicit request =>

    val parts: List[Part] = Part.findAll

    Ok(views.html.parts.list(parts))
  }

  def show(id: Long) = Action { implicit request =>

    Part.findById(id).map {part =>
      Ok(views.html.parts.details(part))
    }.getOrElse(NotFound)

  }

  def newPart = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      partForm.bind(request.flash.data)
    else
      partForm

    Ok(views.html.parts.editPart(form))
  }

  def save = Action { implicit request =>
    val newPartForm = partForm.bindFromRequest()

    newPartForm.fold(
      hasErrors = {form =>
        Redirect(routes.Parts.newPart()).
          flashing(Flash(form.data) +
          ("error" -> Messages("validation.errors")))
      },

      success = {newPart =>
        Part.add(newPart)
        Redirect(routes.Parts.show(newPart.id)).flashing("success" -> Messages("validation.success"))
      }

    )

  }

  private val partForm: Form[Part] = Form(
    mapping(
      "id" -> longNumber.verifying("validation.id.duplicate", Part.findById(_).isEmpty),
    "title" -> nonEmptyText,
    "url" -> nonEmptyText
    )(Part.apply)(Part.unapply)
  )

}
