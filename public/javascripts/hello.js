function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);


    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }

}

var vidId;

$('#embedCode').ready(function () {
    vidId = getId($('#vidUrl').attr('rawVidUrl'));

    $('#embedCode').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + vidId + '" frameborder="0" allowfullscreen></iframe>');
});

